package com.eeema.ramdonco.repository;

import android.util.Log;

import com.eeema.ramdonco.BuildConfig;
import com.eeema.ramdonco.model.rest.RandomUsers;
import com.eeema.ramdonco.model.view.RandomUserVM;
import com.eeema.ramdonco.repository.remote.APIRest;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by emaleavil on 2/7/17.
 */

public class Repository implements Datasource {

    private static final String TAG = Repository.class.getSimpleName();

    Datasource local;
    APIRest rest;

    public Repository(APIRest rest, Datasource local){
        this.rest = rest;
        this.local = local;
    }

    @Override
    public void delete(String username) {
        local.delete(username);
    }

    @Override
    public void favorite(String username, boolean fav) {
        local.favorite(username,fav);
    }

    @Override
    public RandomUserVM user(String username) {
        return null;
    }

    @Override
    public List<RandomUserVM> users() {

        rest.users(BuildConfig.API_URL)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
        .subscribe(new SingleObserver<RandomUsers>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                d.dispose();
            }

            @Override
            public void onSuccess(@NonNull RandomUsers randomUsers) {
                addUsers(randomUsers);
                users();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(TAG, "Users cannot retrieved");
            }
        });

        return local.users();
    }

    @Override
    public void addUsers(RandomUsers users) {
        local.addUsers(users);
    }
}
