package com.eeema.ramdonco.repository;

import android.support.annotation.UiThread;

import com.eeema.ramdonco.model.rest.RandomUsers;
import com.eeema.ramdonco.model.view.RandomUserVM;

import java.util.List;

/**
 * Created by emaleavil on 1/7/17.
 */

public interface Datasource {

    @UiThread
    void delete(String username);

    @UiThread
    void favorite(String username, boolean fav);

    @UiThread
    RandomUserVM user(String username);

    @UiThread
    List<RandomUserVM> users();

    @UiThread
    void addUsers(RandomUsers users);
}
