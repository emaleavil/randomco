package com.eeema.ramdonco.repository.remote;

import com.eeema.ramdonco.model.rest.RandomUsers;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by emaleavil on 2/7/17.
 */

public interface APIRest {

    @GET
    Single<RandomUsers> users(@Url String url);
}
