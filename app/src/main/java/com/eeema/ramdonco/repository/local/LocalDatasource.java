package com.eeema.ramdonco.repository.local;

import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.util.Log;

import com.eeema.ramdonco.model.database.RandomUser;
import com.eeema.ramdonco.model.database.RandomUser_Table;
import com.eeema.ramdonco.model.database.RandomUsersDB;
import com.eeema.ramdonco.model.rest.RandomUsers;
import com.eeema.ramdonco.model.view.RandomUserVM;
import com.eeema.ramdonco.repository.Datasource;
import com.eeema.ramdonco.utils.mapper.MapperUtils;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.raizlabs.android.dbflow.structure.database.transaction.ProcessModelTransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.List;

import static com.raizlabs.android.dbflow.sql.language.SQLite.select;

/**
 * Created by emaleavil on 2/7/17.
 */

public class LocalDatasource implements Datasource {

    private static final String TAG = LocalDatasource.class.getSimpleName();
    private final DatabaseDefinition dbDefinition;

    private LocalDatasource(){
        dbDefinition =  FlowManager.getDatabase(RandomUsersDB.class);
    }

    public void delete(String username) {
        RandomUser user = retrieveUser(username);
        user.deleted = true;
        storeElement(user);
    }

    public void favorite(String username, boolean fav) {
        RandomUser user = retrieveUser(username);
        user.favorite = fav;
        storeElement(user);
    }

    @UiThread @Nullable
    public RandomUserVM user(String username) {
        RandomUser dbUser = retrieveUser(username);
        return MapperUtils.map(dbUser);
    }

    @UiThread @Nullable
    public List<RandomUserVM> users() {
        List<RandomUser> users = select()
                .from(RandomUser.class)
                .where(RandomUser_Table.deleted.isNot(true))
                .queryList();

        return MapperUtils.map(users);
    }

    @Override
    public void addUsers(RandomUsers users) {
     //// TODO: 2/7/17
    }


    private RandomUser retrieveUser(String username){
        return select()
                .from(RandomUser.class)
                .where(RandomUser_Table.username.is(username))
                .and(RandomUser_Table.deleted.isNot(true))
                .querySingle();
    }


    private <T extends BaseModel> void storeList(List<T> elements){
        executeTransaction(new ProcessModelTransaction.Builder<T>((element, wrapper) -> {
            element.save();
            Log.i(TAG,"storing element : " + element.toString());
        })
        .addAll(elements)
        .build());
    }

    private <T extends BaseModel> void storeElement(T element){
        executeTransaction(new ProcessModelTransaction.Builder<T>((element2Store, wrapper) -> {
            element2Store.save();
            Log.i(TAG,"storing element : " + element.toString());
        })
        .add(element)
        .build());
    }

    private <T extends BaseModel> void executeTransaction(ProcessModelTransaction<T> processModelTransaction) {
        Transaction transaction = dbDefinition.beginTransactionAsync(processModelTransaction).error(
                (transaction1, error) -> {
                    error.printStackTrace();
                    Log.e(TAG,"Error en la transaccion: " + error.getMessage());
                })
                .success(transaction1 -> {
                    Log.i(TAG,"Transaccion correcta");
                })
                .build();
        transaction.execute();
    }
}
