package com.eeema.ramdonco;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by emaleavil on 2/7/17.
 */

public class RandomUserApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(new FlowConfig.Builder(this)
                .openDatabasesOnInit(true).build());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        FlowManager.destroy();
    }
}
