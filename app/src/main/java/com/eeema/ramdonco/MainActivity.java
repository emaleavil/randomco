package com.eeema.ramdonco;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.eeema.ramdonco.list.ListUserFragment;

public class MainActivity extends AppCompatActivity {


    public static final String CURRENT_FRAGMENT = "currentFragment";
    public static final int FRAGMENT_LIST = 0;
    public static final int FRAGMENT_DETAIL = 1;

    private int currentFragmentIndex = FRAGMENT_DETAIL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null){

        } else {
            Integer fragmentIndex = savedInstanceState.getInt(CURRENT_FRAGMENT,FRAGMENT_LIST);
            showFragment(fragmentIndex);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_FRAGMENT, currentFragmentIndex);
    }

    private void showFragment(Integer fragmentIndex) {
        switch (fragmentIndex) {
            case FRAGMENT_LIST:
                loadFragmentList();
                break;
            case FRAGMENT_DETAIL:
                loadFragmentDetail();
                break;
        }
    }

    private void loadFragmentDetail() {
        currentFragmentIndex = FRAGMENT_DETAIL;
        getSupportFragmentManager().beginTransaction().add(R.id.container, ListUserFragment.newInstance(FRAGMENT_LIST)).commit();
    }

    private void loadFragmentList() {
        currentFragmentIndex = FRAGMENT_LIST;
        getSupportFragmentManager().beginTransaction().add(R.id.container, ListUserFragment.newInstance(FRAGMENT_LIST)).addToBackStack(null).commit();
    }




}
