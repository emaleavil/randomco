package com.eeema.ramdonco.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eeema.ramdonco.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListUserFragment extends Fragment {


    @BindView(R.id.list_users)
    RecyclerView usersList;

    public static Fragment newInstance(int index){
        Fragment fragment = new ListUserFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("index", index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_user,container,false);
        ButterKnife.bind(this,v);

        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        usersList.setLayoutManager(manager);


        return v;
    }
}
