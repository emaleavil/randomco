package com.eeema.ramdonco.list.adapter.actions;

import com.eeema.ramdonco.repository.Datasource;

/**
 * Created by emaleavil on 1/7/17.
 */

public class ActionMakeFavUser implements Action{

    private Datasource repository;
    private String username;
    private boolean favorite = false;

    public ActionMakeFavUser(Datasource repo){
        this.repository = repo;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public void setFavorite(boolean favorite) { this.favorite = favorite;}

    @Override
    public void execute() {
        repository.favorite(username, favorite);
    }
}
