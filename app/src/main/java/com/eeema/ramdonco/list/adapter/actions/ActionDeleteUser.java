package com.eeema.ramdonco.list.adapter.actions;

import com.eeema.ramdonco.repository.Datasource;

/**
 * Created by emaleavil on 1/7/17.
 */

public class ActionDeleteUser implements Action{

    private Datasource repository;
    private String username;

    public ActionDeleteUser(Datasource repo){
        this.repository = repo;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public void execute() {
        repository.delete(username);
    }
}
