package com.eeema.ramdonco.list.adapter.actions;

import com.eeema.ramdonco.repository.Repository;

/**
 * Created by emaleavil on 3/7/17.
 */

public class ActionRetrieveUsers implements Action {
    private Repository repository;

    public ActionRetrieveUsers(Repository repo){
        this.repository = repo;
    }

    @Override
    public void execute() {
        repository.users();
    }
}
