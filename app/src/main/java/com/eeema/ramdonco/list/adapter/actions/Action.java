package com.eeema.ramdonco.list.adapter.actions;

/**
 * Created by emaleavil on 1/7/17.
 */

public interface Action {

    void execute();
}
