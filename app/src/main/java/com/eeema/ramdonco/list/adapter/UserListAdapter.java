package com.eeema.ramdonco.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.eeema.ramdonco.R;
import com.eeema.ramdonco.list.adapter.actions.ActionDeleteUser;
import com.eeema.ramdonco.list.adapter.actions.ActionMakeFavUser;
import com.eeema.ramdonco.model.view.RandomUserVM;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by emaleavil on 1/7/17.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    private List<RandomUserVM> users;
    private ActionDeleteUser actionDelete;
    private ActionMakeFavUser actionFavorite;

    public UserListAdapter(List<RandomUserVM> users, ActionDeleteUser actionDelete, ActionMakeFavUser actionFavorite){
        this.users = users;
        this.actionDelete = actionDelete;
        this.actionFavorite = actionFavorite;
    }

    @Override
    public UserListAdapter.UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item,parent, false);
        return new UserHolder(view);
    }

    @Override
    public void onBindViewHolder(UserListAdapter.UserHolder holder, int position) {
        final RandomUserVM user = users.get(position);

        Glide.with(holder.profileImage.getContext()).asBitmap().load(user.getPicture()).into(holder.profileImage);
        holder.email.setText(user.getEmail());
        holder.phone.setText(user.getPhone());
        holder.favorite.setChecked(user.isFavorite());
        holder.username.setText(user.getFullName());

        holder.delete.setOnClickListener(v -> {

            actionDelete.setUsername(user.getUsername());
            actionDelete.execute();
            users.remove(position);
            notifyItemRemoved(position);

        });

        holder.favorite.setOnCheckedChangeListener((v, checked) -> {

            actionFavorite.setUsername(user.getUsername());
            actionFavorite.setFavorite(checked);
            user.setFavorite(checked);
            notifyItemChanged(position);

        });

    }


    @Override
    public int getItemCount() {
        return this.users.size();
    }

    static class UserHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.favorite_ck)
        CheckBox favorite;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.user_email)
        TextView email;
        @BindView(R.id.user_name)
        TextView username;
        @BindView(R.id.user_phone)
        TextView phone;
        @BindView(R.id.profile_image)
        CircleImageView profileImage;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}



