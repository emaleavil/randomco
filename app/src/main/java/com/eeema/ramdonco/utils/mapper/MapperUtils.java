package com.eeema.ramdonco.utils.mapper;

import com.eeema.ramdonco.model.database.RandomUser;
import com.eeema.ramdonco.model.view.RandomUserVM;

import java.util.List;

/**
 * Created by emaleavil on 2/7/17.
 */

public class MapperUtils {


    public static RandomUserVM map(RandomUser user){
        Mapper<RandomUser, RandomUserVM> mapper = DBUserToViewModelMapper.create();
        return mapper.transform(user);
    }

    public static List<RandomUserVM> map(List<RandomUser> users){
        GenericListMapper< DBUserToViewModelMapper,RandomUser, RandomUserVM> mapper = GenericListMapper.create(DBUserToViewModelMapper.create());
        return mapper.transform(users);
    }
}
