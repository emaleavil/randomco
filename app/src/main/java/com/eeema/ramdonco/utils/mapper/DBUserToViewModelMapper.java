package com.eeema.ramdonco.utils.mapper;

import com.eeema.ramdonco.model.database.RandomUser;
import com.eeema.ramdonco.model.view.RandomUserVM;

/**
 * Created by emaleavil on 2/7/17.
 */

public class DBUserToViewModelMapper implements Mapper<RandomUser,RandomUserVM> {


    private DBUserToViewModelMapper(){}

    public static DBUserToViewModelMapper create(){
        return new DBUserToViewModelMapper();
    }

    @Override
    public RandomUserVM transform(RandomUser user) {

        String username = user.username;
        String fullname = String.format("", user.name, user.surname);
        String email = user.email;
        String phone = user.phone;
        String picture = user.picture;
        Boolean favorite = user.favorite;

        return new RandomUserVM(username,fullname,email,picture,phone,favorite);
    }

}
