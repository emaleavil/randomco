package com.eeema.ramdonco.utils.mapper;

/**
 * Created by 0013440 on 15/02/2017.
 */

public interface Mapper<T, E> {
  E transform(T object);
}
