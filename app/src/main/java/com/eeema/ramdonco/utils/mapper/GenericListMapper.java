package com.eeema.ramdonco.utils.mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 0013440 on 07/03/2017.
 */

public class GenericListMapper<M extends Mapper<I,O>,I,O> implements Mapper<List<I>, List<O>> {

  private M mapper;

  private GenericListMapper(M mapper){
    this.mapper = mapper;
  }

  public static <M extends Mapper> GenericListMapper create(M mapper){
    return new GenericListMapper(mapper);
  }

  @Override public List<O> transform(List<I> items) {
    List<O> outputList = new ArrayList<>();
    if(items == null) return outputList;

    for (I item : items) {
      O output = mapper.transform(item);
      outputList.add(output);
    }

    return outputList;
  }
}
