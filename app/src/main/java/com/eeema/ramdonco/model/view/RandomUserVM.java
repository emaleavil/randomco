package com.eeema.ramdonco.model.view;

/**
 * Created by emaleavil on 30/6/17.
 */

public class RandomUserVM {

    private String fullName;
    private String email;
    private String picture;
    private String phone;
    private boolean favorite = false;
    private boolean deleted = false;
    private String username;

    public RandomUserVM(String username, String fullName, String email, String picture, String phone, boolean favorite) {
        this.fullName = fullName;
        this.email = email;
        this.picture = picture;
        this.phone = phone;
        this.favorite = favorite;
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getPicture() {
        return picture;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public boolean isDeleted(){
        return deleted;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getUsername() {
        return username;
    }
}
