package com.eeema.ramdonco.model.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by emaleavil on 2/7/17.
 */

@Database(name = RandomUsersDB.NAME, version = RandomUsersDB.VERSION)
public class RandomUsersDB {
    public static final String NAME = "RandomUsersDB";
    public static final int VERSION = 1;
}
