package com.eeema.ramdonco.model.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.annotation.Unique;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by emaleavil on 2/7/17.
 */

@Table(database = RandomUsersDB.class)
public class RandomUser extends BaseModel {

    @PrimaryKey(autoincrement = true)
    public long id;

    @Column @Unique
    public String username;

    @Column
    public String gender;

    @Column @Unique
    public String email;

    @Column
    public String registrationDate;

    @Column
    public String phone;

    @Column
    public String cell;

    @Column
    public String name;

    @Column
    public String surname;

    @Column
    public String picture;

    @Column
    public String street;

    @Column
    public String city;

    @Column
    public String state;

    @Column(defaultValue = "0")
    public Boolean favorite;

    @Column(defaultValue = "0")
    public Boolean deleted;

    public RandomUser(){}

    public RandomUser(long id, String username, String gender, String email, String registrationDate, String phone, String cell, String name, String surname, String picture, String street, String city, String state, Boolean favorite, Boolean deleted) {
        this.id = id;
        this.username = username;
        this.gender = gender;
        this.email = email;
        this.registrationDate = registrationDate;
        this.phone = phone;
        this.cell = cell;
        this.name = name;
        this.surname = surname;
        this.picture = picture;
        this.street = street;
        this.city = city;
        this.state = state;
        this.favorite = favorite;
        this.deleted = deleted;
    }
}
