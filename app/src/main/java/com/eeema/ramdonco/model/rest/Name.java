package com.eeema.ramdonco.model.rest;

/**
 * Created by emaleavil on 30/6/17.
 */

public class Name {

    private String title;
    private String first;
    private String last;

    public String getTitle() {
        return title;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }
}
