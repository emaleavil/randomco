package com.eeema.ramdonco.model.rest;

/**
 * Created by emaleavil on 30/6/17.
 */

public class Picture {

    private String large;
    private String medium;
    private String thumbnail;

    public String getLarge() {
        return large;
    }

    public String getMedium() {
        return medium;
    }

    public String getThumbnail() {
        return thumbnail;
    }
}
