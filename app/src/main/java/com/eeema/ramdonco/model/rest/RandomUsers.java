package com.eeema.ramdonco.model.rest;

import java.util.List;

/**
 * Created by emaleavil on 1/7/17.
 */

public class RandomUsers {

    List<RandomUser> users;

    public RandomUsers(List<RandomUser> users) {
        this.users = users;
    }

    public List<RandomUser> getUsers() {
        return users;
    }
}
