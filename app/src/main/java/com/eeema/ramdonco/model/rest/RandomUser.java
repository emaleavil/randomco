package com.eeema.ramdonco.model.rest;

import android.graphics.Picture;

/**
 * Created by emaleavil on 30/6/17.
 */

public class RandomUser {

    private String gender;
    private String email;
    private String registered;
    private String dob;
    private String phone;
    private String cell;
    private String nat;
    private Location location;
    private Name name;
    private Login login;
    private Picture picture;

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getRegistered() {
        return registered;

    }

    public String getDob() {
        return dob;
    }

    public String getPhone() {
        return phone;
    }

    public String getCell() {
        return cell;
    }

    public String getNat() {
        return nat;
    }

    public Location getLocation() {
        return location;
    }

    public Name getName() {
        return name;
    }

    public Login getLogin() {
        return login;
    }

    public Picture getPicture() {
        return picture;
    }
}
