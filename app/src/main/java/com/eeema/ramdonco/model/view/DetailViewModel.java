package com.eeema.ramdonco.model.view;

/**
 * Created by emaleavil on 30/6/17.
 */

public class DetailViewModel {

    private String gender;
    private String fullName;
    private String location;
    private String registeredDate;
    private String email;
    private String picture;

    public DetailViewModel(String gender, String fullName, String location, String registeredDate, String email, String picture) {
        this.gender = gender;
        this.fullName = fullName;
        this.location = location;
        this.registeredDate = registeredDate;
        this.email = email;
        this.picture = picture;
    }

    public String getGender() {
        return gender;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLocation() {
        return location;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public String getEmail() {
        return email;
    }

    public String getPicture() {
        return picture;
    }
}
